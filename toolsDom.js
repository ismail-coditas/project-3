const htmlTool = `   <h1 class="head1">Tools details</h1>


<div class="form-container">
    <form class="formtools">
            <h2 >Tools Form</h2>

                <div>
                    <label for="toolName">Tool Name</label>                            
                    <input type="text" name="toolName" id="toolName" placeholder="Enter the Tool Name">
                </div>
                
                <div>
                    <label for="toolSize">Tool Size</label>                            
                    <input type="text" name="toolSize" id="toolSize" placeholder="Enter the Tool Size">
                </div>
                
                <div>
                    <label for="toolPrice">Tool Price</label>                            
                    <input type="text" name="toolPrice" id="toolPrice" placeholder="Enter Tool Price">
                </div>
              
                
                
                <button type="submit" id="submitBtn">Submit</button>


                <!-- <button type="button" id="createBtn">create</button>
                <button type="button" class="deleteBtn">Delete</button>
                 -->


    </form>
            
</div>


<!--Table-->
            
<div class="tabletoolscontainer">
<table class="tabletools">
    <thead>
        <tr>
            <th>TOOL NAME</th>
            <th>TOOL SIZE</th>
            <th>TOOL PRICE</th>
        </tr>
    </thead>
    
    <tbody id="tbodytools"></tbody>
    
</table>

</div>`;

document.body.innerHTML = htmlTool;